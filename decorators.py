import assistedjson.views
from functools import wraps
import django.conf


def login_required(function):
    def _decorator( func ):
        def inner_decorator(request, *args, **kwargs):
            if not request.user.is_authenticated():
                view = assistedjson.views.JsonView()
                try:
                    url = django.conf.settings.ASSISTEDJSON_LOGGED_OUT_REDIRECT
                except AttributeError:
                    try:
                        url = django.conf.settings.LOGIN_URL
                    except AttributeError:
                        url = None
                if url is not None:
                    view._response.data(("redirect",url))
                    
                try:
                    message = django.conf.settings.ASSISTEDJSON_LOGGED_OUT_MESSAGE
                except AttributeError:
                    message = "User not logged in"
                return view._response.error_and_respond( message )
            else:
                return func( request, *args, **kwargs )
            
        return wraps(func)(inner_decorator)
        
    return _decorator(function)