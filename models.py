import django.db.models

class SerializableModel(django.db.models.Model):
    class Meta:
        abstract = True

    def toDict(self):
        obj = self.__dict__
        # Remove what we don't want
        keys = obj.keys()
        
        for key in keys:
            if key[0] == "_":
                del obj[key]
            else:
                item = obj[key]
                print item, type(item)
                # List?
                if type(item) == list or type(item) == django.db.models.query.QuerySet:
                    if type(item) == django.db.models.query.QuerySet:
                        item = list(item)
                    new = []
                    for subitem in item:
                        try:
                            new.append(subitem.toDict())
                        except:
                            try:
                                new.append(subitem.__str__())
                            except:
                                # Give up...
                                new = item
                                break
                    obj[key] = new
                else:
                    try:
                        obj[key] = item.toDict()
                    except AttributeError:
                        try:
                            obj[key] = item.__str__()
                        except:
                            del obj[key]
        return obj

